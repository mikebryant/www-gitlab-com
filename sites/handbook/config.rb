monorepo_root = File.expand_path('../..', __dir__)

#--------------------------------------
# Monorepo-related configuration
#--------------------------------------

# TODO: See if these settings can be eliminated or moved to the monorepo extension
set :layout, 'layout' # For some reason this has to be made explicit in the sub-site build, even though it's the default
set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

require_relative '../../extensions/monorepo.rb'
activate :monorepo do |monorepo|
  monorepo.site = 'handbook'
end

#--------------------------------------
# End of Monorepo-related configuration
#--------------------------------------

#---------------------------------------
# Rest of middleman config for this site
#---------------------------------------

# hack around relative requires elsewhere in the common code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

require_relative "../../extensions/breadcrumbs"
require_relative "../../extensions/codeowners"
require_relative '../../extensions/only_debugged_resources'
require_relative "../../lib/homepage"
require 'lib/mermaid'
require 'lib/plantuml'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, {
  format: :xhtml
}

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :codeowners

if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end

# Build-specific configuration
configure :build do
  set :build_dir, "#{monorepo_root}/public"

  # NOTE: Currently no css or js in individual sites, it should all be at top level

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true

  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup
end

# TODO: Add this back when rest of handbook besides /marketing and /engineering is moved to sites/handbook/source
# # Hiring chart pages
# Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
#   proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
# end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

# Don't include the following into the sitemap
ignore '**/.gitkeep'
