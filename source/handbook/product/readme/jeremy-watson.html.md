---
layout: markdown_page
title: "Jeremy Watson's README"
---

## Jeremy's README

Hi, I’m Jeremy. I’m currently a Group Product Manager for the Manage stage. This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 
* A big note of gratitude to external contributors who have already added to this page: [William](https://about.gitlab.com/company/team/#williamchia) and [Luca](https://about.gitlab.com/company/team/#tipyn).

## About me

* I grew up in the Sacramento area of California (go [Kings](https://twitter.com/SacramentoKings)!) and now live in Philadelphia, PA in a little house in the Fishtown neighborhood. I like [taking photos](https://www.instagram.com/dearwatson/) and cooking things for other people.
* I try my best to be actively helpful and kind. 
* I like understanding how systems and people work. My parents described me as a quiet, intense kid that liked to take things around the house apart (particularly our VCR player) and put them back together. 
* I’m a great deal less serious as an adult, but I still like decomposing things to better understand them. This means I ask “why?” and “what’s the problem we’re trying to solve?” frequently.
* I try to listen first before acting and always listen deeply. I hope to be fully present in every conversation so I can develop deep empathy for people and their challenges.
* I have many strong opinions, but they’re [weakly held](https://blog.codinghorror.com/strong-opinions-weakly-held/). I love finding a new perspective that makes me change my opinion, because it means that I’ve gotten a chance to learn something substantive and new. 
* I still struggle with imposter syndrome.

While all of GitLab’s values are extremely important to me, our values of [collaboration](https://about.gitlab.com/handbook/values/#collaboration), [diversity, inclusion and belonging](https://about.gitlab.com/handbook/values/#diversity--inclusion), and [transparency](https://about.gitlab.com/handbook/values/#transparency) are particularly dear to me.

Although performance feedback is typically [not public by default](https://about.gitlab.com/handbook/communication/#not-public), in the spirit of transparency I am choosing to share [my most recent 360 feedback from January 2019](https://drive.google.com/open?id=1h8VrRxkfNewCja1BVb9-_Yhu0WSucKAK) here. I may or may not share future 360 reviews as I always want others to be able to provide me with candid feedback, especially about areas for improvement, without fear that their words (even anonymously) may be made public. 

## My role as Group Manager

If you’re on my team, I’m here to support you, provide focus and clarity on our most important problems, and to advocate for you and the things you ship. I serve you; you work for GitLab - not me - and we should optimize the things we do for the organization, not a particular person or team.

First and foremost, I’m here to help you shine by enabling your success in your current role. I hope to do this in a few ways:
* Provide you with context. I do a lot of communicating with people you might not be otherwise exposed to, and I hope to shield you from things that aren’t as important and provide you with clarity on the things that are.
* Pair with you on problems. You’ll know your area of the product better than I do, but I’ll be close enough to have some informed thoughts of my own.
* Help you directly whenever you need it. I love hearing “I could really use help with…”. The best approach we can take together is preventing stress and anxiety from happening at all at work, but if I will always pick up a hose and firefight with you if needed and you provide that space for me.

I also want to be an avid supporter of your career path. Your career belongs to you, and I want to cheerlead and support you on your path - whether it’s continuing deeper into product management, people management, or not:
* Evangelize your success and watch you shine. I’ll do my best to celebrate your wins to the rest of GitLab.
* Expose you to opportunities that allow you to grow and learn and encourage you to seize them. I don’t want you to be bored!

## How you can help me

* Provide GitLab with thoughtful, good work. Tell me if there's something preventing you from this, and we'll work together on fixing it.
* Default to action. Instead of waiting, move problems forward with a merge request or issue by default.
* Communicate. If there's a problem or you're blocked on something, please bring it up. I'll generally interpret silence as "everything's fine".
  * Don’t save urgent matters for a 1x1. Please bring them to my attention on Slack or by scheduling a separate call.
* Transparency is very important to me. Please default to using public channels.
* I like explicit asks. I’m better at helping when I have a good idea of what you need. “Take a look” is less helpful than “I’m looking for feedback on X and Y, by end of week”.
* Bring your [whole self to work](https://www.forbes.com/sites/hennainam/2018/05/10/bring-your-whole-self-to-work/#6cf3b5526291).

## My working style

* At times, I struggle to find a balance between my personal life and my work life. This is an area of growth for me, and I’m grateful to others when they chastise me for working too much.
* No weekend work. If you find yourself feeling pressure to work on the weekend, please bring it up with me immediately so we can solve for the root cause of that pressure together. 
* Unless specifically mentioned, I don’t expect immediate responses from anyone. I [respect others’ time](https://about.gitlab.com/handbook/communication/#be-respectful-of-others-time) and want you to have the space for a thoughtful response.
  * I don’t keep Slack on my phone in an effort to stay disconnected from work during down time. Please consider doing the same.

## What I assume about others

* [Positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent).
* You’re the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/). You’re better at your job than I am! You know best, and you’ll tell me if there’s something preventing you from doing your best work possible.
  * If I disagree with you, I may try to steer you down a different path. But me disagreeing with you doesn’t mean that you’re doing something wrong as long as you’re collaborating effectively with me and our teammates.
* You’ll ask for my input and help if it’s needed. 
* Work is absolutely not the most important thing in your life.  I assume that there are [cherished relationships](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second) and personal interests that are more important to your happiness, and it’s hard to be happy with your job when work detracts from those things. 

## What I want to earn

* Your trust.
* Your psychological safety. I want to allow you the space to take risks, be vulnerable with me, and tell me when I could be doing something better.
* Your unfiltered honesty. Disagree with me and ask me hard questions. We grow the most by challenging each other and being intellectually honest together about the things we know and don’t know.

## Communicating with me

* If we have a recurring 1x1, that time belongs to you. These interactions are very important to me and are dedicated to whatever topic you’re interested in discussing with me. They’re not status meetings unless you’d like them to be.
* I tend to make suggestions and asks, never commands. 
  * This can make my input unclear. If I’m not communicating expectations effectively, please say so and I’ll do my best to adjust my style. If I have a strong opinion about something, I will say so.
* I try to express gratitude frequently. This is genuine.
* I try to be a structured communicator and thinker, but sometimes start to speak in an unstructured way when I get excited about something.
  * Please interject if you’re not getting what you want out of me.
* Sometimes, my calendar will be packed with meetings. Please DM me on Slack if you can’t find time, I will always make time for you.
