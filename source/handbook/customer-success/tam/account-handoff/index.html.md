---
layout: markdown_page
title: "Account Hand-Off TAM-to-TAM Checklist"
---

TAMs will need to occasionally transfer accounts they have been working with to another TAM (e.g. a TAM changes territories, a realignment occurs in Sales, a need to equalize books of business, etc.), and they should use this handbook page to help guide them through important questions and topics during the handoff.

Below are checkpoints during the account handoff process that TAMs can use to keep track of information they will need in order to successfully transition accounts.

### Account Handoff CTA

The first step once you are aware of a handoff is for the new TAM to open an [CTA in Gainsight](https://about.gitlab.com/handbook/customer-success/tam/gainsight/#ctas). This is where you will track completion of tasks necessary to a successful handoff. Be sure to select the "Account Handoff" playbook and assign the relevant tasks to the previous and new TAM.

Next, you will open an issue in the [TAM project](https://gitlab.com/gitlab-com/customer-success/tam) and use the [Account Handoff template](https://gitlab.com/gitlab-com/customer-success/tam/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) and fill out the appropriate details. This issue will be the source of collaboration for any discussion items, questions, notes, etc. that pop up throughout the handoff process.

As you follow the tasks in the CTA and collaborate in the issue, be aware of the following suggestions to ensure a seamless handoff experience.

### Topics to Cover

#### License Evaluation

Discuss the state of the customer's license, such as:

* Active users count
* True-up assessment
* Upcoming renewal

#### Communication Evaluation

One of the most important parts of a handoff is excellent transparent and timely communication. It's important for the new TAM to have a thorough understanding of the communication channels used to interact with the customer. The following is not an exhaustive list, but should cover the major channels:

* The customer's preferred method of communicating
* Most recent correspondence
* Past critical conversations
* Meeting [cadence calls](/handbook/customer-success/tam/cadence-calls/) frequency and notes
* Regular email cadence
* [Collaboration project](/handbook/customer-success/tam/engagement/) activity
* Slack channel activity (internal and/or external)
* Chorus recordings if applicable and helpful

#### Health Score Evaluation

Review their [health score](/handbook/customer-success/tam/health-score-triage/) and discuss the history of the relationship, as well as what the previous TAM anticipates from the customer.

Also review the customer's product sentiment and their feature requests, specifically: if they are up to date, are they prioritized, how many have been shipped, etc.

#### Support Ticket Evaluation

Thoroughly understand the main technical issues the customer cares about and has had problems with. Take note of critical open tickets and collaborate with support as needed, and review any key technical issues the customer experienced in the past; for example, migrations, integrations, major upgrades, etc.

#### Installation Evaluation

Assess the environment where the customer's GitLab instance is hosted.

* Self-Hosted Assessment
  * Health Checks
  * Architecture diagrams
  * Machine specifications
  * GitLab configuration files

* GitLab.com (SaaS) Assessment
  * Runners Evaluation
    * Private On-Prem Runners
  * CI Minutes Utilization
    * CI usage trends(i.e daily, weekly, monthly etc.)
